{
  description = "Docker development shell";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    systems.url = "github:nix-systems/default";
  };

  outputs = inputs @ { self, flake-parts, ... }: flake-parts.lib.mkFlake { inherit inputs; } {
    systems = import inputs.systems;

    perSystem = { config, self', inputs', pkgs, system, lib, ... }: {
      devShells.default = pkgs.mkShellNoCC {
        name = "docker-shell";

        packages = [
          pkgs.docker
          pkgs.docker-compose
        ];
      };
    };
  };
}
