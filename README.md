## To start

docker-compose up --remove-orphans --force-recreate --build

## To stop and clean

docker-compose stop
docker-compose down --volumes

## Test

- Tomcat 8, App1: http://127.0.0.1:8080/sample/
- Tomcat 8, App2: http://127.0.0.1:8080/foobar/
- Tomcat 9, App1: http://127.0.0.1:9090/sample/
- Tomcat 9, App2: http://127.0.0.1:9090/foobar/

## Nix files (flake.*)

The Nix files are here to provide `docker` and `docker-compose` for computers
that don't have them installed.

Use the command `nix develop` to enter a ephemeral shell with `docker` and
`docker-compose``.
